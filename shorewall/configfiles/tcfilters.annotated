#
# Shorewall -- /etc/shorewall/tcfilters
#
# For information about entries in this file, type "man shorewall-tcfilters"
#
# See https://shorewall.org/traffic_shaping.htm for additional information.
#
######################################################################################
# 
# Entries in this file cause packets to be classified for traffic shaping.
# 
# Beginning with Shorewall 4.4.15, the file may contain entries for both IPv4 and
# IPv6. By default, all rules apply to IPv4 but that can be changed by inserting
# a line as follows:
# 
# IPV4
# 
#     Following entries apply to IPv4.
# 
# IPV6
# 
#     Following entries apply to IPv6
# 
# ALL
# 
#     Following entries apply to both IPv4 and IPv6. Each entry is processed
#     twice; once for IPv4 and once for IPv6.
# 
# The columns in the file are as follows (where the column name is followed by a
# different name in parentheses, the different name is used in the alternate
# specification syntax).
# 
# CLASS - interface:class
# 
#     The name or number of an interface defined in shorewall-tcdevices(5)
#     followed by a class number defined for that interface in
#     shorewall-tcclasses(5).
# 
# SOURCE - {-|address|+ipset}
# 
#     Source of the packet. May be a host or network address. DNS names are not
#     allowed. Beginning with Shorewall 4.6.0, an ipset name (prefixed with '+')
#     may be used if your kernel and ip6tables have the Basic Ematch capability
#     and you set BASIC_FILTERS=Yes in shorewall.conf (5). The ipset name may
#     optionally be followed by a number or a comma separated list of src and/or
#     dst enclosed in square brackets ([...]). See shorewall-ipsets(5) for
#     details.
# 
# DEST - {-|address|+ipset}
# 
#     Destination of the packet. May be a host or network address. DNS names are
#     not allowed. Beginning with Shorewall 4.6.0, an ipset name (prefixed with
#     '+') may be used if your kernel and ip6tables have the Basic Ematch
#     capability and you set BASIC_FILTERS=Yes in shorewall.conf (5). The ipset
#     name may optionally be followed by a number or a comma separated list of
#     src and/or dst enclosed in square brackets ([...]). See shorewall-ipsets(5)
#     for details.
# 
#     You may exclude certain hosts from the set already defined through use of
#     an exclusion (see shorewall-exclusion(5)).
# 
# PROTO - {-|{protocol-number|protocol-name|all}[,...]}
# 
#     Protocol.
# 
#     Beginning with Shorewall 4.5.12, this column can accept a comma-separated
#     list of protocols.
# 
# DPORT - [-|port-name-or-number]
# 
#     Optional destination Ports. A Port name (from services(5)) or a port number
#     ; if the protocol is icmp, this column is interpreted as the destination
#     icmp-type(s).
# 
#     This column was previously labelled DEST PORT(S).
# 
# SPORT - [-|port-name-or-number]
# 
#     Optional source port.
# 
#     This column was previously labelled SOURCE PORT(S).
# 
# TOS (Optional) - [-|tos]
# 
#     Specifies the value of the TOS field. The tos value can be any of the
#     following:
# 
#       □ tos-minimize-delay
# 
#       □ tos-maximize-throughput
# 
#       □ tos-maximize-reliability
# 
#       □ tos-minimize-cost
# 
#       □ tos-normal-service
# 
#       □ hex-number
# 
#       □ hex-number/hex-number
# 
#     The hex-numbers must be exactly two digits (e.g., 0x04)x.
# 
# LENGTH - [-|number]
# 
#     Optional - Must be a power of 2 between 32 and 8192 inclusive. Packets with
#     a total length that is strictly less than the specified number will match
#     the rule.
# 
# PRIORITY - [-|priority]
# 
#     Added in Shorewall 4.5.8. Specifies the rule priority. The priority value
#     must be > 0 and <= 65535.
# 
#     When a priority is not given:
# 
#       □ For Shorewall versions prior to 4.5.8 - all filters have priority 10.
# 
#       □ For Shorewall 4.5.8 and later - for each device, the compiler maintains
#         a high-water priority with an initial value of 0. When a filter has no
#         priority, the high-water priority is incremented by 1 and assigned to
#         the filter. When a priority greater than the high-water priority is
#         entered in this column, the high-water priority is set to the specified
#         priority. An attempt to assign a priority value greater than 65535
#         (explicitly or implicitly) raises an error.
# 
#     The default priority values used by other Shorewall-generated filters are
#     as follows:
# 
#       □ Classify by packet mark - ( class priority << 8 ) | 20.
# 
#       □ Ingress policing - 10
# 
#       □ Simple TC ACK packets - 1
# 
#       □ Complex TC ACK packets - ( class priority << 8 ) | 10.
# 
#       □ Classify by TOS - ( class priority << 8 ) | 15.
# 
#       □ Class with 'occurs' - 65535
# 
# Example
# 
# IPv4 Example 1:
# 
#     Place all 'ping' traffic on interface 1 in class 10. Note that ALL cannot
#     be used because IPv4 ICMP and IPv6 ICMP are two different protocols.
# 
#            #CLASS    SOURCE    DEST         PROTO   DPORT
# 
#            IPV4
# 
#            1:10      0.0.0.0/0 0.0.0.0/0    icmp    echo-request
#            1:10      0.0.0.0/0 0.0.0.0/0    icmp    echo-reply
# 
#            IPV6
# 
#            1:10      ::/0      ::/0         icmp6   echo-request
#            1:10      ::/0      ::/0         icmp6   echo-reply
# 
# IPv4 Example 2:
# 
#     Add two filters with priority 10 (Shorewall 4.5.8 or later).
# 
#            #CLASS    SOURCE    DEST         PROTO   DPORT           PRIORITY
# 
#            IPV4
# 
#            1:10      0.0.0.0/0 0.0.0.0/0    icmp    echo-request    10
#            1:10      0.0.0.0/0 0.0.0.0/0    icmp    echo-reply      10
# 
# IPv6 Example 1:
# 
#     Add two filters with priority 10 (Shorewall 4.5.8 or later).
# 
#            #CLASS    SOURCE    DEST         PROTO   DPORT           PRIORITY
# 
#            IPV6
# 
#            1:10      ::/0      ::/0         icmp    echo-request    10
#            1:10      ::/0      ::/0         icmp    echo-reply      10
# 
######################################################################################
#CLASS		SOURCE		DEST		PROTO	DPORT	SPORT	TOS	LENGTH
